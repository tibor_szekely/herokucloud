const express = require('express')
const app = express()
// const port = process.env.port || 3000

app.set('port', process.env.PORT || 3000);

app.get('/', (req, res) => {
  res.send('Hello world!')
})

app.listen(app.get('port'), () => {
  console.log(`Example app listening at http://localhost:...`)
})